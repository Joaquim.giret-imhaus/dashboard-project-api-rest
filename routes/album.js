var express = require('express');
var router = express.Router();

// we import our user controller
var album = require('../controllers/album.controller');

/* GET albums listing. */
router.get('/', album.findAll);

/* GET genres for all albums */
router.get('/genres', album.findAllGenres)

module.exports = router;