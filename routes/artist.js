var express = require('express');
var router = express.Router();

// we import our user controller
var artist = require('../controllers/artist.controller');

/* GET artists listing. */
router.get('/', artist.findAll);

/* GET followers for all artists */
router.get('/followers', artist.findAllFollowers);
router.get('/nextBirthday', artist.findNextBirthday);

/* PUT a new artist */
router.put('/', artist.create);

module.exports = router;
