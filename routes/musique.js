var express = require('express');
var router = express.Router();

// we import our user controller
var music = require('../controllers/musique.controller');

/* GET albums listing. */
router.get('/', music.findAll);

/** GET music title with matching streams */
router.get('/streams',music.findMusicStream);



module.exports = router;