var express = require('express');
var router = express.Router();

// we import our user controller
var event = require('../controllers/event.controller');

/* GET albums listing. */
router.get('/', event.findAll);
router.get('/date', event.findByDate);


module.exports = router;