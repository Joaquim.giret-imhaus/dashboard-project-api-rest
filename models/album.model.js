const mongoose = require('mongoose');

const albumSchema = new mongoose.Schema(
  {
    AlbumName: String,
    AlbumGenre: String,
    AlbumCover: String,
    AlbumDate: String,
    Songs: Array
  },
  {
      collection: "Albums"
  }
);

module.exports = mongoose.model('Album', albumSchema);