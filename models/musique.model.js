const mongoose = require('mongoose');

const musiqueSchema = new mongoose.Schema(
  {
    MusicTitle: String,
    MusicDuration: String,
    MusicStream: Number,
    MusicLike: Number
  },
  {
      collection: "Musique"
  }
);

module.exports = mongoose.model('Musique', musiqueSchema);