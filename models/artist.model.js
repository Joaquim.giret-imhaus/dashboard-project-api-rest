const mongoose = require('mongoose');

const artistSchema = new mongoose.Schema(
  {
    Name: String,
    Birthday: String,
    Followers: Number,
    Album: Array
  },
  {
      collection: "Artiste"
  }
);

module.exports = mongoose.model('Artist', artistSchema);