const mongoose = require('mongoose');

const eventSchema = new mongoose.Schema (
    {
        EventType: String,
        EventArtiste: String,
        Location: String,
        EventDate: String
    },
    {
        collection: "Evenement"
    }
)

module.exports = mongoose.model('Event', eventSchema);
