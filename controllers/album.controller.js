const Album = require('../models/album.model');
const _ = require('lodash');

// Retrieve and return all Albums from the database.
exports.findAll = (req, res) => {
  Album.find({})
    .then(albums => {
        console.log("albums", albums);
      res.status(200).json(albums);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving the albums.'
      });
    });
};

// Retrieve and return the count list of all genres for all albums from the database.
exports.findAllGenres = (req, res) => {
  Album.find({})
    .then(albums => {
        const allGenres = _.map(albums, function(o){
          var result = _.pick(o, ['AlbumGenre']);
          return result;
        })

        var existingGenres = ['Reggae', 'Hip Hop', 'Classical', 'Rap', 'Rock', 'House', 'Electro'];

        var result = _.map(existingGenres,
            function(existingGenres) {
                var length = _.reject(allGenres, function(el){
                    return (el.AlbumGenre.indexOf(existingGenres) < 0);
                }).length;

                return {AlbumGenre: existingGenres, count: length};
            }
        );

      res.status(200).json(result);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving the genres.'
      });
    });
};