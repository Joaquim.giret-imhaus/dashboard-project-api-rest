const Music = require('../models/musique.model');
const _ = require('lodash');

// Retrieve and return all Albums from the database.
exports.findAll = (req, res) => {
  Music.find({})
    .then(musiques => {
        console.log("Musique", musiques);
      res.status(200).json(musiques);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving the musiques.'
      });
    });
};

// Retrieve and return all Albums from the database.
exports.findMusicStream = (req, res) => {
  Music.find({})
    .then(musiques => {
      const allStreams=_.map(musiques,function (o){
        var result = _.pick(o,['MusicTitle','MusicStream']);
        return result ;

      })
        console.log("Stream", allStreams);
      res.status(200).json(allStreams);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving the musiques.'
      });
    });
};