const Event = require('../models/event.model');
const _ = require('lodash');

// Retrieve and return all Artists from the database.
exports.findAll = (req, res) => {
  Event.find({})
    .then(events => {
        console.log("events", events);
      res.status(200).json(events);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || 'Some error occurred while retrieving events.'
      });
    });
};

exports.findByDate = (req,res)=>{
    Event.find({})
    .then(events =>{ 
            const byDate = _.map(events, function(o){
            var result = _.pick(o, ['EventType', 'EventArtiste','Location','EventDate']);
            return result;})
        
        const sortedbyDate = _.orderBy(byDate,function(e){return e.EventDate},['desc'])
        console.log("events", events);
        res.status(200).json(sortedbyDate);
      })

      .catch(err => {
        res.status(500).send({
          message: err.message || 'Some error occurred while retrieving events.'
        });
      });
     
};
